﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParseTheParcel.Models
{
    public class ParcelCostCalculateService : IParcelCostCalculateService
    {      
        public decimal CalculateCost(Parcel parcel)
        {
            List<Rate> lookup = Packages.GetParcelSizesInDescendingOrder();
            int i = 0;
            decimal cost = 0m;
            foreach (var rate in lookup)
            {
                
                if (parcel.Dimensions.Length <= lookup[i].Dimensions.Length
                    && parcel.Dimensions.Breadth <= lookup[i].Dimensions.Breadth
                    && parcel.Dimensions.Height <= lookup[i].Dimensions.Height)
                {
                    cost = lookup[i].Cost;
                }
                i++;
            }
            return cost;
        }
    }
}
