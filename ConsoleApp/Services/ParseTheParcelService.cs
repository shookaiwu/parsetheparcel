using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParseTheParcel.Models;

namespace ParseTheParcel.Services
{
    public class ParseTheParceService : IParseTheParceService
    {
        public void ParseTheParce()
        {
            Console.WriteLine("Enter the weight of your parcel: ");
            double weight = Convert.ToDouble(Console.ReadLine());
            if (weight > 0)
            Console.Write("You typed ");
            Console.Write(Convert.ToString(weight));

            IParcelWeightCheckService weightRepository = new ParcelWeightCheckService();
            var maxWeight = weightRepository.GetMaxWeight();
            if ( weight > maxWeight)
            {
                Console.Write("kg, Max weight is ");
                Console.Write(maxWeight);
                Console.Write("kg.");
            }
            else
            {
                Console.Write("kg. ");

                IParcelDimensionCheckService dimensionsRepository = new ParcelDimensionCheckService();
                
                var maxLength = dimensionsRepository.GetMaxDimension().Length;
                var maxBreadth = dimensionsRepository.GetMaxDimension().Breadth;
                var maxHeight = dimensionsRepository.GetMaxDimension().Height;

                Console.Write("Enter the Length of your parcel: ");
                double length = Convert.ToDouble(Console.ReadLine());
                Console.Write("You typed ");
                Console.Write(length);
                Console.Write("mm. ");

                if (length > maxLength)
                {
                    Console.Write(" Max Length is ");
                    Console.Write(maxLength);
                    Console.Write("mm. ");
                }
                else
                {
                    Console.Write("Enter the Breadth of your parcel: ");
                    double breadth = Convert.ToDouble(Console.ReadLine());
                    Console.Write("You typed ");
                    Console.Write(breadth);
                    Console.Write("mm. ");

                    if (breadth > maxBreadth)
                    {
                        Console.Write(" Max Breadth is ");
                        Console.Write(maxBreadth);
                        Console.Write("mm. ");
                    }
                    else
                    {
                        Console.Write("Enter the Height of your parcel: ");
                        double height = Convert.ToDouble(Console.ReadLine());
                        Console.Write("You typed ");
                        Console.Write(height);
                        Console.Write("mm. ");

                        if (height > maxHeight)
                        {
                            Console.Write(" Max Height is ");
                            Console.Write(maxHeight);
                            Console.Write("mm. ");
                        }else
                        {
                            decimal cost = 0m;
                            Parcel parcel = new Parcel(new Dimensions(length, breadth, height), weight);
                            IParcelCostCalculateService CalculateCostRepository = new  ParcelCostCalculateService();
                            
                            cost = CalculateCostRepository.CalculateCost(parcel);
                            Console.Write(" Cost is $");
                            Console.Write(cost);
                        }
                    }
                    
                }
            }
        }
    }
}