﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParseTheParcel.Models;

namespace ParseTheParcel.Services
{
    public class ParcelDimensionCheckService : IParcelDimensionCheckService
    {      
        public Dimensions GetMaxDimension()
        {
            return Packages.GetParcelSizesInDescendingOrder().First().Dimensions;
        }
    }
}
