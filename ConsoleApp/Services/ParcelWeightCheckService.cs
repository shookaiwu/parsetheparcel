﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParseTheParcel.Models;

namespace ParseTheParcel.Services
{
    public class ParcelWeightCheckService : IParcelWeightCheckService
    {
        private const double MaxWeight = 25;

        public bool WeightCheck(Parcel parcel)
        {
            return parcel.Weight > MaxWeight;
        }

        public double GetMaxWeight()
        {
            return MaxWeight;
        }
    }
}
