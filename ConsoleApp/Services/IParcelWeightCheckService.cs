﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParseTheParcel.Models;

namespace ParseTheParcel.Services
{
    interface IParcelWeightCheckService
    {
        bool WeightCheck(Parcel parcel);
        double GetMaxWeight();
    }
}
