﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParseTheParcel.Models
{
    public  class Parcel
    {
        public Dimensions Dimensions { get; set; }
        public double Weight { get; set; }

        public Parcel(Dimensions dimensions, double weight)
        {
            Dimensions = dimensions;
            Weight = weight;
        }
    }
}
