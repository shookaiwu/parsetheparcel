﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParseTheParcel.Models
{
    public class Rate
    {
        public string PackageType { get; set; }
        public Dimensions Dimensions { get; set; }
        public decimal Cost { get; set; }

        public Rate(string packageType, Dimensions dimensions,decimal cost)
        {
            PackageType = packageType;
            Dimensions = dimensions;
            Cost = cost;
        }
    }
}
