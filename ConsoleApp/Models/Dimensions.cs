﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParseTheParcel.Models
{
    public class Dimensions
    {
        public double Length { get; set;}
        public double Breadth { get; set; }
        public double Height { get; set; }

        public Dimensions(double length, double breadth, double height)
        {
            Length = length;
            Breadth = breadth;
            Height = height;
        }
    }
}
