﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParseTheParcel.Models
{
    public static class Packages
    {
        public static List<Rate> GetParcelSizesInDescendingOrder()
        {
            List<Rate> rates = new List<Rate>()
            {
                new Rate ( "Large", new Dimensions(400, 600, 250),  8.50m),
                new Rate ( "Medium", new Dimensions(300, 400, 200), 7.50m),
                new Rate ( "Small", new Dimensions(200, 300, 150), 5.00m)
            };      
            return rates;
        }
            
    }
}
