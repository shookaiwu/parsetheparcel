﻿using System;
using ParseTheParcel.Models;
using ParseTheParcel.Services;

namespace ParseTheParcel
{
    class Program
    {
        static void Main(string[] args)
        {
            IParseTheParceService service = new ParseTheParceService();
            service.ParseTheParce();
        }
    }
}
